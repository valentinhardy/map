class Canvas {

  constructor(element, options = {}) {
    this.element = element
    this.options = Object.assign({}, {
      container: document.getElementById('canvasDiv'),
      paint: false
    }, options)
    this.canvasHeight = 'auto'
    this.canvasWidth = '100%'
    this.clickX = new Array()
    this.clickY = new Array()
    this.clickDrag = new Array()


    //create canvas div width style etc
    this.canvas = document.createElement('canvas') //vrai canvas

    this.canvas.setAttribute('id', 'canvas')
    this.canvas.style.width = this.canvasHeight
    this.canvas.style.height = this.canvasHeight
    this.context = this.canvas.getContext('2d') //context
    this.options.container.appendChild(this.canvas)

    this.clear()

    //je suis censé dessiner
    this.canvas.addEventListener('mousedown', (e) => {
      this.context.beginPath()
      this.options.paint = true
      this.addClick(e.offsetX, e.offsetY, false)
      this.context.closePath()
    })

    //la souris est up donc paint est false
    this.canvas.addEventListener('mouseup', (e) => {
      this.options.paint = false
    })

    //la souris bouge
    this.canvas.addEventListener('mousemove', (e) => {
      if (this.options.paint === true) {
        this.addClick(e.offsetX, e.offsetY, true)
        this.redraw()
      }
    })
    //la souris n'est plus dans la div
    this.canvas.addEventListener('mouseleave', (e) => {
      this.options.paint = false
    })
    //end constructor
  }

  addClick(x, y, dragging) {
    this.clickX.push(x)
    this.clickY.push(y)
    this.clickDrag.push(dragging)
  }
  //clear the canvas
  clear() {
    $("#validate_canvas").click( () => {
      if (this.clickX.length >= 30) {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.beginPath();
        this.canvas.addEventListener('mousedown', (e) => {
          this.canvas.width = this.canvas.width;
        })
      }
    })
  }

  redraw() {
    let context = this.context
    let canvas = this.canvas
    let click = this.clickX.length
    context.strokeStyle = "black";
    context.lineJoin = "round"
    context.lineWidth = 1.2

    let i
    for (let i = 1; i < this.clickX.length; i++) {
      context.beginPath()
      context.moveTo(this.clickX[i - 1], this.clickY[i - 1])
      if (this.clickDrag[i])
        context.lineTo(this.clickX[i], this.clickY[i])
      context.stroke()
      context.closePath()
    }
    if (click >= 30) {
    $('#validate_canvas').removeClass('disabled')
    }
  }
  //end class
}


//new Canvas
document.addEventListener("DOMContentLoaded", function() {

  new Canvas(document.querySelector("canvas"), {})

})
