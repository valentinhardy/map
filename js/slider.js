class Carousel {
 /**
 * @param {HTMLElement} element
 * @param {Object} options
 * @param {Object} options.slidesToScroll elements à faire défiler
 * @param {Object} options.slidesVisible elements visibles
 * @param {Object} options.loop Doit-on boucler ou pas ?
 */
    constructor (element, options = {}) {
        this.element = element
        this.options = Object.assign({}, {
            slidesToScroll : 1,
            slidesVisible: 1,
            loop: false
        }, options)

        let children = [].slice.call(this.element.children)
        this.currentItem = 0
        this.root = this.createDivWithClass('carousel')
        this.container = this.createDivWithClass('carousel__container')
        this.root.appendChild(this.container)
        this.element.appendChild(this.root)
        this.moveCallbacks = []
        this.items = children.map((child)  => {
            let item = this.createDivWithClass('carousel__item')
            item.appendChild(child)
            this.container.appendChild(item)
            return item
        })
        this.setStyle()
        this.startSlider()
        this.stopSlider()
        this.createNavigation()
        this.moveCallbacks.forEach(cb => cb(0))
        $('.carousel__play').css('display', 'none')
        $('.carousel__stop').css('display', 'block')

        document.addEventListener('keyup', e => {
          if (e.key === 'ArrowRight' || e.key == 'Right'){
            this.next()
            $('.carousel__play').css('display', 'block')
            $('.carousel__stop').css('display', 'none')
            clearInterval(this.autoSlide);
          } else if (e.key === 'ArrowLeft'  || e.key == 'Left'){
            this.prev()
            $('.carousel__play').css('display', 'block')
            $('.carousel__stop').css('display', 'none')
            clearInterval(this.autoSlide);
          }
        })
    }


//autoSlide
//play & stop
startSlider (){
  this.autoSlide = setInterval(this.next.bind(this), 2000)
  let playButton = this.createButton('carousel__play waves-effect waves-light green')
  this.root.appendChild(playButton)
    playButton.addEventListener('click', e => {
      this.autoSlide = setInterval(this.next.bind(this), 2000)
    $('.carousel__stop').css('display', 'block')
    $('.carousel__play').css('display', 'none')
    clearInterval(this.autoSlideButton)
    clearInterval(this.repriseAutoSlideButton)
    })
  }
stopSlider(){
  let stopButton = this.createButton('btn-small waves-effect waves-light carousel__stop red lighten-1')
  this.root.appendChild(stopButton)
  stopButton.addEventListener('click', e => {
    $('.carousel__stop').css('display' ,'none')
    clearInterval(this.autoSlide)
    clearInterval(this.autoSlideButton)
    clearInterval(this.repriseAutoSlideButton)
    $('.carousel__play').css('display', 'block')
  })
}

createNavigation() {
  //navigation
  let nextButton = this.createDivWithClass('carousel__next large material-icons waves-effect')
  let prevButton = this.createDivWithClass('carousel__prev large material-icons waves-effect')
  this.root.appendChild(nextButton)
  this.root.appendChild(prevButton)
  nextButton.addEventListener('click', (e) =>{
    clearInterval(this.repriseAutoSlideButton)
    clearInterval(this.autoSlide)
    this.next()
    $('.carousel__play').css('display', 'block')
    $('.carousel__stop').css('display', 'none')

  })
  prevButton.addEventListener('click', (e) => {
    clearInterval(this.autoSlideButton)
    clearInterval(this.autoSlide)
    this.prev()
    $('.carousel__play').css('display', 'block')
    $('.carousel__stop').css('display', 'none')
    })

  if(this.options.loop === false){
    return
  }
  this.onMove(index => {
    if (index === 0){
      prevButton.classList.add('carousel__prev--hidden')
    } else {
      prevButton.classList.remove('carousel__prev--hidden')
    }
    if (this.items[this.currentItem + this.options.slidesVisible] === undefined) {
      nextButton.classList.add('carousel__next--hidden')
    } else {
      nextButton.classList.remove('carousel__next--hidden')
    }
  })

}

next ()  {
  this.gotoItem(this.currentItem + this.options.slidesToScroll)
}

prev ()  {
  this.gotoItem(this.currentItem - this.options.slidesToScroll)
}



/**
 * Déplace le carousel vers l'élément ciblé
 * @param (number) index
*/

gotoItem (index)  {
  if(index < 0) {
    index = this.items.length - this.options.slidesVisible
  } else if ( index >= this.items.length || this.items [this.currentItem + this.options.slidesVisible] === undefined && index >
  this.currentItem) {
    index = 0
  }
  let translateX = index * -100 / this.items.length
  this.container.style.transform = 'translate3d('+ translateX  +'%, 0, 0)'
  this.currentItem = index
  this.moveCallbacks.forEach(cb => cb(index))
}



/**
* @param {string} className
* @returns {HTMLElement}
*/


onMove(cb){
  this.moveCallbacks.push(cb)
}


/**
  * Applique les bonnes dimensions aux elements
*/

setStyle()  {
  let ratio = this.items.length / this.options.slidesVisible
  this.container.style.width = (ratio * 100) + "%"
  this.items.forEach(item => item.style.width = ((100 / this.options.slidesVisible) / ratio) + "%")
}



/**
 * @param {String} className
 * @returns {HTMLElement}
 **/

  createDivWithClass (className) {
      let div = document.createElement('div');
      div.setAttribute('class', className);
      return div;
  }
  createButton(className){
      let button = document.createElement('button')
      button.setAttribute('class', className)
      return button
    }
}


document.addEventListener('DOMContentLoaded', function(){

    new Carousel(document.querySelector('#carousel1'), {
        slidesVisible: 1,
        slidesToScroll: 1,
        loop: true
    })

})
