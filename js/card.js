class Map {

  constructor(idElement, options = {}) {


    this.options = Object.assign({}, {
      style: 'mapbox.streets',
      mapContainer: L.map(idElement).setView([45.758000, 4.840000], 15),
      init: L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        minZoom: 14,
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoidmhhcmR5NTkyODAiLCJhIjoiY2pwaWEwOG1xMTF2NDNrbWQydHFtaXdtMiJ9.sV9xCPEuLhv0HbRMcc-znA'
      }),
      data: "https://api.jcdecaux.com/vls/v1/stations?contract=Lyon&apiKey=44200bd32e236ff0e2a3ea346cd5d6fb365136d6",
    }, options)

    //get infos stations by url
    let xmlHttp = new XMLHttpRequest();
    let data;
    let url = this.options.data
    xmlHttp.open("GET", url, false);
    xmlHttp.send(null);
    this.data = xmlHttp.responseText;

    //call functions
    this.addOptionstomap()
    this.addMarkers()
    this.checkOut()
    this.modal()
    this.timer()
    console.log(this)
  }

  //add container options
  addOptionstomap() {
    this.options.init.addTo(this.options.mapContainer)
  }
  //add markers & user infos
  addMarkers() {
    let stations = JSON.parse(this.data)
    let i
    for (i = 0; i < stations.length; i++) {

      //une station
      let station = stations[i];

      //renseigne la localisation des markers
      let localisation = stations[i].position
      let nameStation = station.name

      //add markers by positions
      //test leaflet icons
      this.blueIcon = new L.Icon({
        iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png',
        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
        iconSize: [50, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
      });
      this.markers = L.marker(localisation).addTo(this.options.mapContainer)
      this.markers.info = stations[i]
      this.markers.bindPopup(`<p class="replace">${nameStation}</p>`).closePopup();

      //recup les infos de la station au click marker et les affiche
      this.markers.addEventListener('click', (e) => {
        this.infoStation = e.target.info
        $('#form').removeClass('is--hidden')
        $('#form').addClass('is--visible')
        $('#validate_form').removeClass('disabled')
        $('#openModal').addClass("disabled")
        //remplacement des chaines open/close
        let stationStatus = this.infoStation.status
        stationStatus = stationStatus.replace('OPEN', 'La station est ouverte.')
        console.log(stationStatus)
        if (stationStatus === 'CLOSE') {
          machaine.replace('CLOSE', 'La station est fermée.')
        }

        //hide form if available bikes === 0
        this.form = document.getElementById('checkout')
        if (this.infoStation.available_bikes <= 0) {
          this.form.style.display = "none"
        } else {
          this.form.style.display = "block"
        }
        //display infos stations in #userinfo
        let html = '<div id="infostation_id"><p>' + this.infoStation.name + ' <br> ' + this.infoStation.address + '</p>' + '<p class="select"> Vélos disponibles : ' + this.infoStation.available_bikes + '</p></li>' + '<li class="status"><p>' + stationStatus + '</p></li>'
        this.saveDiv = $('#userinfo')
        this.saveDiv.html(html);
      })
    }
  }

  //Local & Session Storage // regEx // verifie si nom + prenom est dans le storage & enregistre
  checkOut() {
    let checkout = document.getElementById('openModal')
    let displayInfo = document.getElementById('validate_canvas')
    //stock le nom + prenom
    document.getElementById('prenom').value = localStorage.getItem('prenom')
    document.getElementById('nom').value = localStorage.getItem('nom')
    checkout.addEventListener('click', (e) => {
      if ('nom' && 'prenom' in localStorage) {
        console.log("Votre nom et votre prénom sont sauvegardés vous pouvez maintenant signer et reserver.")
      }
    })


    //regEx
    let validation = document.getElementById('validate_form');
    let prenom = document.getElementById('prenom');
    let nom = document.getElementById('nom');
    let prenom_m = document.getElementById('prenom_manquant')
    let nom_m = document.getElementById('nom_manquant')
    let prenom_v = /^[a-zA-ZéèîïÉÈÏÎ][a-zéèàçîï]+([-'\s][a-zA-ZéèîïÉÈÏÎ][a-zéèêàçîï]+)?/;
    let nom_v = /^[a-zA-ZéèîïÉÈÏÎ][a-zéèàçîï]+([-'\s][a-zA-ZéèîïÉÈÏÎ][a-zéèêàçîï]+)?/;
    validation.addEventListener('click', f_valid);

    function f_valid(e) {
      if (prenom.validity.valueMissing || nom.validity.valueMissing) {
        //bloque l'envoi du formulaire
        e.preventDefault();
        prenom_m.textContent = "Entrez votre prénom!"
        nom_m.textContent = "Entrez votre nom!"
        prenom_m.style.color = "red"
        nom_m.style.color = "red"
      } else if (prenom_v.test(prenom.value) == false || nom_v.test(nom.value) == false) {
        e.preventDefault()
        console.log('mort')
        prenom_m.textContent = "Format incorrect"
        nom_m.textContent = "Format incorrect"
        prenom_m.style.color = 'orange'
        nom_m.style.color = 'orange'
      } else {
        e.preventDefault()
        validation_info.textContent = "Ok!"
        validation_info.style.color = "green"
        $('.rappel').addClass('is--hidden')
        $('#validate_form').addClass('disabled')
        $('#openModal').removeClass('disabled')
      }
      //fin regEx
    }
    displayInfo.addEventListener('click', (e) => {
      // enregistre dans le session storage la station reservée et affiche dans info_adress
      sessionStorage.setItem('infoAddress', this.save)
      if ('infoAddress' in sessionStorage) {
        this.infoSave = $('#info_address')
        this.save = '<p>' + 'Vélo\'ve reservé à la station N°' + this.infoStation.name + ', ' + this.infoStation.address + ' par ' + localStorage.nom + ' ' + localStorage.prenom + ' pour une durée de ' + '</p>'
        this.infoSave.value = sessionStorage.setItem('infoAddress', this.save)
        this.infoSave.html(this.save)
        $('#infoTimer').addClass('is--visible')
        $('#infoTimer').removeClass('is--hidden')
        console.log('Votre station est désormais enregistrée pour une durée de 20min')
      }
    })
  }
  //ouvre/ferme la modale
  modal() {
    //ouvre et ferme la modale de signature
    this.open = document.getElementById('openModal')
    this.launch = document.getElementById('validate_canvas')
    this.close = document.getElementById('close__toggle')
    this.modal = document.getElementById('modal__container')
    this.open.addEventListener('click', () => {
      this.modal.style.transform = 'translateY(50%)'
    })
    this.launch.addEventListener('click', () => {
      this.modal.style.transform = 'translateY(100%)'
      $('#openModal').addClass('disabled')

    })
    this.close.addEventListener('click', () => {
      this.modal.style.transform = 'translateY(100%)'
    })
  }
  //timer card
  timer() {
    $(document).ready(function() {
      let minutes = 19
      let secondes = 60
      let on = false
      let reset = false
      let timerID
      //demarre le timer
      $("#validate_canvas").click(function() {
        Start()
        $('#infoTimer').css('display', 'block')
        if (secondes < 60) {
          minutes = 19
          secondes = 59
        }
      })
      //calcul
      function calcul() {

        // decrement seconde
        if (secondes <= 60) {
          secondes--
        }

        if (secondes < 0) {
          secondes = 59
          minutes -= 1
        }

        if (minutes < 0) {
          this.containerTimer = document.getElementById('infoTimer')
          clearInterval(timerID)
          this.containerTimer.style.display = "none"
          sessionStorage.removeItem('infoAddress')
        }
        $("#minutes").html(minutes+' : ');
        $("#secondes").html(secondes);
        if (secondes < 10) {
          $("#minutes").html(minutes+' : ');
          $("#secondes").html(" 0"+secondes);        }
      }

      function Start() {
        if (on === false) {
          timerID = setInterval(calcul, 1000)
          on = true
        }
      }
    });
  }
  //crée une div au besoin
  createDivWithClass(className) {
    let div = document.createElement('div');
    div.setAttribute('class', className);
    return div;
  }
  //crée un bouton
  createButton(className) {
    let button = document.createElement('button')
    button.setAttribute('class', className)
    return button
  }
}

document.addEventListener('DOMContentLoaded', function() {

  window.onload = ('#timer')
  new Map('card', {
    layout: L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      minZoom: 14,
      maxZoom: 18,
      id: 'mapbox.streets',
      accessToken: 'pk.eyJ1IjoidmhhcmR5NTkyODAiLCJhIjoiY2pwaWEwOG1xMTF2NDNrbWQydHFtaXdtMiJ9.sV9xCPEuLhv0HbRMcc-znA'
    }),
    style: 'mapbox.streets',
    data: "https://api.jcdecaux.com/vls/v1/stations?contract=Lyon&apiKey=44200bd32e236ff0e2a3ea346cd5d6fb365136d6"
  })
})
